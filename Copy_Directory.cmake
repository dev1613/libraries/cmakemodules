include(CMakeParseArguments)

function(Copy_Directory)

  # Parse arguments
  set(options)
  set(oneValueArgs SRC_DIR DEST_DIR PATTERN)
  set(multiValueArgs)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

  # Must have source and destination
  if(NOT ARG_SRC_DIR)
    message(FATAL_ERROR "Copy_Directory must have SRC_DIR arg")
  endif()

  if(NOT ARG_DEST_DIR)
    message(FATAL_ERROR "Copy_Directory must have DEST_DIR arg")
  endif()

  # Get pattern (all files by default)
  if(NOT ARG_PATTERN)
    set(ARG_PATTERN "*")
  endif()

  # Get files in source dir
  file(GLOB_RECURSE COPY_FILES
    LIST_DIRECTORIES false
    RELATIVE ${ARG_SRC_DIR}
    ${ARG_SRC_DIR}/${ARG_PATTERN}
  )

  # Copy to destination dir
  foreach(f ${COPY_FILES})
    configure_file(${ARG_SRC_DIR}/${f} ${ARG_DEST_DIR}/${f} COPYONLY)
  endforeach()

endfunction()