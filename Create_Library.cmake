include(CMakeParseArguments)

function(Create_Library)

  # Parse arguments
  set(options)
  set(oneValueArgs TARGET TYPE)
  set(multiValueArgs SOURCES INCLUDE_DIRS LINK_LIBRARIES)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

  # Must have target
  if(NOT ARG_TARGET)
    message(FATAL_ERROR "Create_Library must have TARGET arg")
  endif()

  # Must have sources
  if(NOT ARG_SOURCES)
    message(FATAL_ERROR "Create_Library must have SOURCES arg")
  endif()

  # Get lib type (default to shared)
  if(NOT ARG_TYPE)
    set(ARG_TYPE SHARED)
  endif()

  # Create library
  add_library(${ARG_TARGET} ${ARG_TYPE} ${ARG_SOURCES})

  # Include directories
  set(${ARG_TARGET}_INCLUDE_DIRS ${ARG_INCLUDE_DIRS} CACHE STRING "${ARG_TARGET} include directories")
  target_include_directories(${ARG_TARGET} PRIVATE ${ARG_INCLUDE_DIRS})

  # Link libraries
  target_link_libraries(${ARG_TARGET} PUBLIC ${ARG_LINK_LIBRARIES})

endfunction()