unset(QT_INCLUDE_DIRS)
unset(QT_LIBRARIES)

# Find Qt6 or Qt5
find_package(QT NAMES Qt6 Qt5)
if(QT_FOUND)

  # Initialize dirs
  set(ver_qt Qt${QT_VERSION_MAJOR})

  # Find components
  if(QT_FIND_COMPONENTS)
    find_package(${ver_qt} COMPONENTS ${QT_FIND_COMPONENTS})
    set(QT_FOUND ${${ver_qt}_FOUND})

    foreach(module ${QT_FIND_COMPONENTS})
      list(APPEND QT_INCLUDE_DIRS ${${ver_qt}${module}_INCLUDE_DIRS})
      list(APPEND QT_LIBRARIES ${${ver_qt}${module}_LIBRARIES})
    endforeach()
  endif()
endif()